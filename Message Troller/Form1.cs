﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Message_Troller
{
    public partial class Form1 : Form
    {
        NotifyIcon trayIcon;
        Icon icon;
        Thread messageThread;

        public Form1()
        {
            InitializeComponent();
            GenerateTrayIcon();
        }

        public void GenerateTrayIcon()
        {
            icon = new Icon(@"res\Microsoft.ico");
            trayIcon = new NotifyIcon();

            MenuItem progName = new MenuItem("Notification Manager");
            MenuItem quitItem = new MenuItem("Quit");
            ContextMenu trayMenu = new ContextMenu();
            trayMenu.MenuItems.Add(progName);
            trayMenu.MenuItems.Add(quitItem);
            trayIcon.ContextMenu = trayMenu;

            quitItem.Click += QuitItem_Click;
        }

        private void QuitItem_Click(object sender, EventArgs e)
        {
            messageThread.Abort();
            trayIcon.Dispose();
            this.Close();
        }

        private void HideUI()
        {
            
        }
    }
}
